# pdf_cli is a pdf split,delete,merge cli

[![Crates.io](https://img.shields.io/crates/v/pdf_cli.svg)](https://crates.io/crates/pdf_cli)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/pdf_cli)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/pdf_cli/-/raw/master/LICENSE)

## Examples:

```sh
pdf_cli -f ./demo.pdf delete --pages [1,2] => delete 1,2 pages
pdf_cli -f ./demo.pdf merge --path ./demo_1.pdf => merge the two pdfs create merged.pdf
pdf_cli -f ./demo.pdf split --num 2 => split pdf at page 2 and and create 1..=2 pages to splited.pdf ,3.. pages to demo.pdf
```

## Install

```sh
cargo install pdf_cli
```
